import argparse
import requests
import time
import csv
import subprocess
import urllib3
import json

# Disable SSL verification warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# API endpoint URLs
scan_export_url = "https://{}/8834/scans/{}/export"
status_url = "https://{}/8834/scans/{}/export/{}/status"
download_url = "https://{}/8834/scans/{}/export/{}/download"

# Function to read API credentials from JSON file
def read_api_credentials():
    try:
        with open("api_credentials.json", "r") as file:
            credentials = json.load(file)
            access_key = credentials.get("access_key")
            secret_key = credentials.get("secret_key")
            return access_key, secret_key
    except FileNotFoundError:
        raise Exception("API credentials file not found. Please provide valid credentials in api_credentials.json.")

# Read API credentials from JSON file
access_key, secret_key = read_api_credentials()

# Set up argument parser
parser = argparse.ArgumentParser(description="Nessus Scan Export and Analysis Script")
parser.add_argument("--scan-id", dest="scan_id", type=int, required=True, help="Nessus Scan ID")
parser.add_argument("--name", dest="name", default="mint", help="Name in the URL (default is 'mint')")
args = parser.parse_args()

# Set the request headers
headers = {
    'Content-Type': 'application/json',
    'X-ApiKeys': 'accessKey={};secretKey={}'.format(access_key, secret_key)
}

# Disable SSL verification
verify_ssl = False

# Request export of scan data in CSV format
export_data = {
    "format": "csv"
}

# API call to export scan data
scan_export_url = scan_export_url.format(args.name, args.scan_id)
response = requests.post(scan_export_url, headers=headers, json=export_data, verify=verify_ssl)
response.raise_for_status()

# Extract the file token from the response
file_token = response.json()["file"]

# Check the status of the export file
status_url = status_url.format(args.name, args.scan_id, file_token)

while True:
    response = requests.get(status_url, headers=headers, verify=verify_ssl)
    response.raise_for_status()

    status = response.json()["status"]
    if status == "ready":
        break

    time.sleep(1)  # Wait for 1 second before checking again

# Download the exported file
download_url = download_url.format(args.name, args.scan_id, file_token)

response = requests.get(download_url, headers=headers, verify=verify_ssl)
response.raise_for_status()

# Save the downloaded file
with open('result.csv', 'wb') as file:
    file.write(response.content)

print("File downloaded successfully as result.csv")

# Count the number of lines with risk not passed
counter = 0

# Open the result.csv file
with open('result.csv', 'r') as file:
    # Create a CSV reader
    reader = csv.DictReader(file)

    # Iterate over each row in the CSV file
    for row in reader:
        risk = row['Risk']

        # Check if the value of 'Risk' is not 'PASSED'
        if risk != 'PASSED':
            counter += 1

# Print the counter value
print("Number of lines with risk not passed:", counter)

# Run Ansible playbook if the counter value is greater than 1
if counter > 1:
    playbook = "playbook.yml"
    inventory = "inventory.ini"

    # Run the Ansible playbook using the 'ansible-playbook' command
    command = ["ansible-playbook", "-i", inventory, playbook]
    subprocess.run(command)
